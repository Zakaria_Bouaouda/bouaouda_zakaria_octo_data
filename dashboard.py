import time
import streamlit as st
import requests
from PIL import Image
import numpy as np
from src.constants import INFERENCE_EXAMPLE, CM_PLOT_PATH,BALANCE_GRAPH,AGGREGATOR_MODEL_PATH
from src.training.train_pipeline import TrainingPipeline
from src.eda.main import graph1, graph2,BALANCE_GRAPH,CORR_GRAPH
from src.models.aggregator_model import AggregatorModel
from src.database.main import save_prediction

st.title("Card Fraud Detection Dashboard")
st.sidebar.title("Data Themes")

sidebar_options = st.sidebar.selectbox(
    "Options",
    ("EDA", "Training", "Inference")
)

if sidebar_options == "EDA":
    st.header("Exploratory Data Analysis")

    graph1()

    st.image(Image.open(BALANCE_GRAPH))
    st.info("From the graph, we can concluse that the dataset is imbalanced. Therefore there is a need to make a resampling where we reduce from the size of the class which have more examples.")
    st.info('From now forward, we will resample the data to be balanced in the rest of this page and in training phase.')

    st.info('# Correlation graph')
    graph2()
    st.image(Image.open(CORR_GRAPH))

    st.info("# Observations")
    observations = ["Some features show a very high positive correlation with the class (V2, V4 and V11 for instance)",
"Many more exhibit a large negative correlation (V1, V3, V7, V10, V12, V14, V16-V18)",
"Features themselves might be higlhy correlated.",
"The fact that there is a smaller region in the heatmap that seems to be separate from the rest of it suggest there is some intrinsic organization to the data (clusters of data), features V1-V19 seem to be highly correlated to target and to other features in this group.",
"Correlation matrix of original data points is quite different than that of the sub-sampled data. This is because, the sheer number of non-negative examples is so large, the class features 'looks to be' independent of any feature and is almost always 0. Notice that even in this case some features show strong negative correlation."
]
    for observation in observations:st.info(observation)


elif sidebar_options == "Training":
    st.header("Model Training")
    # st.info("Before you proceed to training your model. Make sure you "
    #         "have checked your training pipeline code and that it is set properly.")

    name = st.text_input('Model name', placeholder='decisiontree')
    print("ll", name)
    serialize = st.checkbox('Save model')
    train = st.button('Train Model')

    if train:
        with st.spinner('Training model, please wait...'):
            time.sleep(1)
            try:
                tp = TrainingPipeline()
                tp.train(serialize=serialize, model_name=name)
                tp.render_confusion_matrix(plot_name=name)
                accuracy, f1 = tp.get_model_perfomance()
                col1, col2 = st.columns(2)

                col1.metric(label="Accuracy score", value=str(round(accuracy, 4)))
                col2.metric(label="F1 score", value=str(round(f1, 4)))

                st.image(Image.open(CM_PLOT_PATH))

            except Exception as e:
                st.error('Failed to train model!')
                st.exception(e)


else:
    st.header("Fraud Inference")
    st.info("This section simplifies the inference process. "
            "You can tweak the values of feature 1, 2, 19, "
            "and the transaction amount and observe how your model reacts to these changes.")
    feature_11 = st.slider('Transaction Feature 11', -10.0, 10.0, step=0.001, value=-4.075)
    feature_13 = st.slider('Transaction Feature 13', -10.0, 10.0, step=0.001, value=0.963)
    feature_15 = st.slider('Transaction Feature 15', -10.0, 10.0, step=0.001, value=2.630)
    amount = st.number_input('Transaction Amount', value=1000, min_value=0, max_value=int(1e10), step=100)
    infer = st.button('Run Fraud Inference')

    INFERENCE_EXAMPLE[11] = feature_11
    INFERENCE_EXAMPLE[13] = feature_13
    INFERENCE_EXAMPLE[15] = feature_15
    INFERENCE_EXAMPLE[28] = amount

    if infer:
        with st.spinner('Running inference...'):
            time.sleep(1)
            try:
                # result = requests.post(
                #     'http://localhost:3333/api/inference',
                #     json=INFERENCE_EXAMPLE
                # )
                model = AggregatorModel()
                model.load(AGGREGATOR_MODEL_PATH)

                features = np.array(INFERENCE_EXAMPLE).reshape(1, -1)
                prediction = model.predict(features)
                result = str(prediction[0])

                if int(result) == 1:
                    st.success('Done!')
                    st.metric(label="Status", value="Transaction: Fraudulent")
                    save_prediction("Fraudulent") ####
                else:
                    st.success('Done!')
                    st.metric(label="Status", value="Transaction: Clear")
                    save_prediction('Clear') #####
            except Exception as e:
                st.error('Failed to call Inference API!')
                st.exception(e)
