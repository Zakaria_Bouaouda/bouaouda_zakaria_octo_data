from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///emi.sqlite3'
db = SQLAlchemy(app)

class Prediction(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    label = db.Column(db.String(80), unique=True)
    # email = db.Column(db.String(120), unique=True)

    def __init__(self, label):
        self.label = label
        # self.email = email

    def __repr__(self):
        return '<Prediction %r>' % self.label

db.create_all()
db.session.commit()

def save_prediction(label):
    l1 = Prediction(label)
    db.session.add(l1)
    # l1 = Prediction('clear')
    # l2 = Prediction('fraud')
    # db.session.add(l1)
    # db.session.add(l2)
    # db.session.commit()
    users = Prediction.query.all()
    print (users)

# save_prediction('hello')