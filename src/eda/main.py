
from src.constants import DATASET_PATH,BALANCE_GRAPH,CORR_GRAPH
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

def graph1():
	df = pd.read_csv(DATASET_PATH)
	plt.hist(df["Class"], bins=2)
	plt.savefig(BALANCE_GRAPH)
	plt.clf()

def graph2():
	df = pd.read_csv(DATASET_PATH)
	# make the data balanced
	df1 = df[df.Class == 1]
	df2 = df[df.Class == 0].sample(500)
	df = pd.concat([df1,df2])
	corrmat = df.corr()
	corrmat_orig = df.corr()
	f, ax = plt.subplots(figsize=(16, 8))
	plt.subplot(1, 2, 1)
	plt.title('Correlation matrix of sub-sampled data')
	sns.heatmap(corrmat, vmax=1, square=True)
	plt.subplot(1, 2, 2)
	plt.title('Correlation matrix of original data')
	sns.heatmap(corrmat_orig, vmax=1, square=True)
	plt.savefig(CORR_GRAPH)
	plt.clf()